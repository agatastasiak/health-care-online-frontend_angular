import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from '@angular/cdk/layout';
import { NavbarModule } from './navbar/navbar.module';
import { FooterComponent } from './footer/components/footer.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [FooterComponent],
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule
  ],
  exports: [
    FlexLayoutModule,
    LayoutModule,
    NavbarModule,
    FooterComponent
  ]
})
export class SharedModule { }
