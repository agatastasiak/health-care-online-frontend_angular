export interface GoogleCoordinates {
    lat: number;
    lng: number;
}