import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SliceEnumJsonService {

  constructor() { }

  keys(e: any): string[] {
    var keys = Object.keys(e);
    var array: string[] = keys.slice(keys.length / 2);
    return array;
  }
}
