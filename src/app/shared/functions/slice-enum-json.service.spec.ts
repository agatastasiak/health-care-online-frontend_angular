import { TestBed } from '@angular/core/testing';

import { SliceEnumJsonService } from './slice-enum-json.service';

describe('SliceEnumJsonService', () => {
  let service: SliceEnumJsonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SliceEnumJsonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
