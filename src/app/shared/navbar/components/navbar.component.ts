import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { OrganizationUnitService } from 'src/app/user/services/organization-unit.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NavbarMenuItemsService } from '../services/navbar-menu-items.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  orgUnitCities: string[];
  city: string;
  specs: string[];

  constructor(
    private organizationUnitService: OrganizationUnitService,
    private route: Router,
    private navbarMenuItemService: NavbarMenuItemsService,
    private translateService: TranslateService

  ) { }

  ngOnInit(): void {
    this.organizationUnitService.findAddressCitiesOfOrgUnits().pipe(
      map(cities => [...new Set(cities)])
    )
      .subscribe(data =>
        this.orgUnitCities = data);

    this.navbarMenuItemService.getDoctorsSpeclizations().subscribe(data =>
      this.specs = data);
  }

  navigate(link: string, cityValue: string): void {
    this.route.navigate([link], {
      queryParams: {
        city: cityValue
      }
    });
  }

}
