import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavbarMenuItemsService {

  private specsUrl: string;

  constructor(private http: HttpClient) {
    this.specsUrl = "http://localhost:8083/api/doctor/specialization";
  }

  getDoctorsSpeclizations() : Observable<string[]>{
    return this.http.get<string[]>(this.specsUrl);
  }
}
