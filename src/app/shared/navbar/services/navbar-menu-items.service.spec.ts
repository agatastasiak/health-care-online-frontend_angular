import { TestBed } from '@angular/core/testing';

import { NavbarMenuItemsService } from './navbar-menu-items.service';

describe('NavbarMenuItemsService', () => {
  let service: NavbarMenuItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NavbarMenuItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
