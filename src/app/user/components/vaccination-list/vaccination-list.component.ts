import { Component, OnInit, ViewChild } from '@angular/core';
import { IVaccination, VaccinationKeys } from './IVaccination';
import { VaccinationServiceService } from '../../services/vaccination.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';



@Component({
  selector: 'app-vaccination-list',
  templateUrl: './vaccination-list.component.html',
  styleUrls: ['./vaccination-list.component.scss']
})
export class VaccinationListComponent implements OnInit {

  vaccinations: IVaccination[] = [];
  displayedColumns: string[];
  dataSource: MatTableDataSource<IVaccination>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private vaccinationService: VaccinationServiceService) { }

  ngOnInit(): void {
    this.vaccinationService.findAll().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    this.displayedColumns = Object.keys(VaccinationKeys);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
