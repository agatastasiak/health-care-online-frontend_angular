export enum VaccinationKeys {
    id = "id",
    vaccinationName = "vaccinationName",
    price = "price",
    minAge = "minAge",
    orgUnit = "orgUnit"
}

export interface IVaccination {
    [VaccinationKeys.id]:string
    [VaccinationKeys.vaccinationName]: string;
    [VaccinationKeys.price]: number;
    [VaccinationKeys.minAge]: number;
    [VaccinationKeys.orgUnit]: string;
}
