
export interface OrganizationUnit {

    id: number;
    name: string;
    imageUrl: string;
    addressStreet: string;
    addressBuildingNumber: string;
    addressFlatNumber: number;
    addressZipCode: string;
    addressCity: string;
    addressCountry: string;
    addressLongitude: number;
    addressLatitude: number;
}
