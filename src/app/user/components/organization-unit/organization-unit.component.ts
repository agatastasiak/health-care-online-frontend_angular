import { Component, OnInit, OnDestroy } from '@angular/core';
import { AddressService } from '../../services/address.service';
import { Address } from '../address/address';
import { OrganizationUnit } from './organization-unit';
import { OrganizationUnitService } from '../../services/organization-unit.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { GoogleMapComponent } from '../google-map/google-map.component';
import { JsonPipe } from '@angular/common';
import { GoogleCoordinates } from '../../../shared/models/google-coordinates.model';

@Component({
  selector: 'app-organization-unit',
  templateUrl: './organization-unit.component.html',
  styleUrls: ['./organization-unit.component.scss']
})
export class OrganizationUnitComponent implements OnInit, OnDestroy {

  orgUnitAddressByAddressId: Address;
  orgUnitByCity: OrganizationUnit[];
  cities: string[];
  city: string;
  sub: Subscription;

  constructor(
    private addressService: AddressService,
    private route: ActivatedRoute,
    private orgUnitService: OrganizationUnitService,
    private dialog: MatDialog
  ) { }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  ngOnInit(): void {
    this.sub = this.route.queryParams.subscribe(params => {
      this.city = params.city;
      this.displayDetailsOfCity(this.city);
    });
  }

  displayDetailsOfCity(city: string) {
    this.orgUnitService.findOrgUnitByCity(city).subscribe(data =>
      this.orgUnitByCity = data);
  }

  openGoogleMap(lat: number, lng: number): void {
    const coordinates: GoogleCoordinates = {
      lat: lat,
      lng: lng
    };
    this.dialog.open(GoogleMapComponent, {
      data: coordinates
    });
  }

}
