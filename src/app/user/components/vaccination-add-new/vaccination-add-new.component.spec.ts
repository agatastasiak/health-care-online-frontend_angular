import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VaccinationAddNewComponent } from './vaccination-add-new.component';

describe('VaccinationAddNewComponent', () => {
  let component: VaccinationAddNewComponent;
  let fixture: ComponentFixture<VaccinationAddNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VaccinationAddNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VaccinationAddNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
