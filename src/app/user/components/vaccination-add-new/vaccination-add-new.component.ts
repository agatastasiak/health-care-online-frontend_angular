import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VaccinationServiceService } from '../../services/vaccination.service';
import { IVaccination } from '../vaccination-list/IVaccination';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-vaccination-add-new',
  templateUrl: './vaccination-add-new.component.html',
  styleUrls: ['./vaccination-add-new.component.scss']
})
export class VaccinationAddNewComponent implements OnInit {

  vaccination: IVaccination;
  firstFormGroup: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private vaccinationService: VaccinationServiceService,
    private _formBuilder: FormBuilder
  ) {
    this.vaccination = {
      minAge: null,
      id: null,
      vaccinationName: null,
      price: null,
      orgUnit: null
    };
  }

  ngOnInit(): void {
    this.firstFormGroup = this._formBuilder.group({
      name: new FormControl("", Validators.compose([Validators.required, Validators.minLength(2), Validators.pattern(new RegExp("[a-zA-Z]"))])),
      price: new FormControl("", Validators.compose([Validators.required, Validators.pattern(new RegExp("\\d", "i"))])),
      minAge: new FormControl("", Validators.compose([Validators.required, Validators.pattern(new RegExp("\\d", "i"))])),
      orgUnit: new FormControl("", Validators.compose([Validators.required, Validators.minLength(2), Validators.pattern(new RegExp("[a-zA-Z0-9]", "i"))]))
    });
  }

  ngOnSubmit() {
    if (this.firstFormGroup.valid) {
      this.vaccination.minAge = this.firstFormGroup.controls.minAge.value;
      this.vaccination.vaccinationName = this.firstFormGroup.controls.name.value;
      this.vaccination.price = this.firstFormGroup.controls.price.value;
      this.vaccination.orgUnit = this.firstFormGroup.controls.orgUnit.value;
      this.vaccinationService.save(this.vaccination).subscribe(_result => this.goToVaccinationsList())
      console.log("form is valid");
    }
  }
  goToVaccinationsList() {
    this.router.navigate(['/vaccination-list'])
  }

  evaluateForm(): boolean {
    if (this.firstFormGroup.untouched) {
      return false;
    }
    if (!this.firstFormGroup.untouched) {
      if (this.firstFormGroup.valid) {
        return true;
      }
    }
  }

}
