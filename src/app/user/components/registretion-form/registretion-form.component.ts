import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Roles } from '../../roles.enum';
import { Genders } from '../../genders.enum';
import { SliceEnumJsonService } from 'src/app/shared/functions/slice-enum-json.service';
import { IUser } from 'src/app/user/IUser';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-registretion-form',
  templateUrl: './registretion-form.component.html',
  styleUrls: ['./registretion-form.component.scss']
})
export class RegistretionFormComponent implements OnInit {

  registrationForm: FormGroup;
  _formBuilder = new FormBuilder();
  user: IUser;
  users: IUser[];
  roles: string[] = [];
  genders: string[] = [];

  constructor(
    private sliceEnumJson: SliceEnumJsonService,
    private userService: UserService) { }

  ngOnInit(): void {
    this.roles = this.sliceEnumJson.keys(Roles);
    this.genders = this.sliceEnumJson.keys(Genders);
    this.createFormGroup();
    this.registrationForm.controls.role.disable();
  }

  ngOnSubmit(): void {
    if (this.registrationForm.valid) {
      this.userService.save(this.registrationForm).subscribe(_singleUser => this.user = _singleUser);
      this.registrationForm.reset();
      this.registrationForm.get('name').clearValidators();
      this.registrationForm.get('surname').clearValidators();
      this.registrationForm.get('email').clearValidators();
      this.registrationForm.get('pesel').clearValidators();
      this.registrationForm.get('addressStreet').clearValidators();
      this.registrationForm.get('addressBuildingNumber').clearValidators();
      this.registrationForm.get('addressFlatNumber').clearValidators();
      this.registrationForm.get('addressZipCode').clearValidators();
      this.registrationForm.get('addressCity').clearValidators();
      this.registrationForm.get('addressCountry').clearValidators();
      this.registrationForm.get('role').clearValidators();
      this.registrationForm.get('sex').clearValidators();
      this.registrationForm.get('birthDate').clearValidators();
      this.registrationForm.get('password').clearValidators();
    }
  }

  findAll(): void {
    this.userService.findAll().subscribe(data => {
      this.users = data;
    })
  }

  private createFormGroup() {
    this.registrationForm = this._formBuilder.group({
      'name': ['', [Validators.required, Validators.minLength(2), Validators.pattern(new RegExp("[a-zA-Z]"))]],
      'surname': ['', [Validators.required, Validators.pattern(new RegExp("[a-zA-Z]"))]],
      'email': ['', [Validators.required, Validators.pattern(new RegExp("^([a-zA-Z0-9_.-]+)@([a-zA-Z0-9_.-]+)\.([a-zA-Z]{2,5})$"))]],
      'pesel': ['', [Validators.required, Validators.pattern(new RegExp("^\\d{11}$"))]],
      'addressStreet': ['', [Validators.required, Validators.compose([Validators.minLength(2), Validators.pattern(new RegExp("[a-zA-Z0-9]"))])]],
      'addressBuildingNumber': ['', [Validators.required, Validators.compose([Validators.minLength(1), Validators.pattern(new RegExp("[a-zA-Z0-9]"))])]],
      'addressFlatNumber': ['', [Validators.required, Validators.compose([Validators.minLength(2), Validators.pattern(new RegExp("[a-zA-Z0-9]"))])]],
      'addressZipCode': ['', [Validators.required, Validators.compose([Validators.minLength(2), Validators.pattern(new RegExp("[0-9]{2}\-[0-9]{3}$"))])]],
      'addressCity': ['', [Validators.required, Validators.pattern(new RegExp("[a-zA-Z0-9]"))]],
      'addressCountry': ['', [Validators.required, Validators.minLength(2)]],
      'role': [this.roles[1], Validators.required],
      'sex': ['', Validators.required],
      'birthDate': ['', Validators.required],
      'password': ['', [Validators.required, Validators.minLength(2)]]
    });
  }

}
