import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistretionFormComponent } from './registretion-form.component';

describe('RegistretionFormComponent', () => {
  let component: RegistretionFormComponent;
  let fixture: ComponentFixture<RegistretionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistretionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistretionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
