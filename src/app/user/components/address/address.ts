export interface Address {

    id:string;
    street: string;
    buildingNumber: number;
    flatNumber: string;
    zipCode: string;
    city: string;
    country: string;
}
