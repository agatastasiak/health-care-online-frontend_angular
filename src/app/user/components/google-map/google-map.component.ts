import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GoogleCoordinates } from '../../../shared/models/google-coordinates.model';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss']
})
export class GoogleMapComponent implements OnInit {

  zoom: number = 14;
  origin: GoogleCoordinates;
  // destination: GoogleCoordinates;

  constructor(@Inject(MAT_DIALOG_DATA) public data: GoogleCoordinates) { }

  ngOnInit(): void {

  }
  
  getCurrentPosition(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.origin = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        }
      })
    }
  }
}

