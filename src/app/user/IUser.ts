import { Roles } from './roles.enum'
import { Genders } from './genders.enum'

export interface IUser {

    name: string;
    surname: string;
    email: string;
    role: Roles;
    pesel: string;
    addressStreet: string;
    addressBuildingNumber: string;
    addressFlatNumber: string;
    addressZipCode: string;
    addressCity: string;
    addressCountry: string;
    sex: Genders;
    birthDate: Date;
}

