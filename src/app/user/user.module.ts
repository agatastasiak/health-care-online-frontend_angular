import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainContentComponent } from './components/main-content/main-content.component';
import { InfoComponent } from './components/info/info.component';
import { MaterialModule } from '../material.module';
import { UserRoutingModule } from './user-routing.module';
import { VaccinationFormComponent } from './components/vaccination-form/vaccination-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VaccinationListComponent } from './components/vaccination-list/vaccination-list.component';
import { VaccinationAddNewComponent } from './components/vaccination-add-new/vaccination-add-new.component';
import { PatientsPortalComponent } from './components/patients-portal/patients-portal.component';
import { RegistretionFormComponent } from './components/registretion-form/registretion-form.component';
import { OrganizationUnitComponent } from './components/organization-unit/organization-unit.component';
import { AddressComponent } from './components/address/address.component';
import { SharedModule } from '../shared/shared.module';
import { GoogleMapComponent } from './components/google-map/google-map.component';
import { AgmCoreModule } from '@agm/core';
import { DoctorsInfoComponent } from './components/doctors-info/doctors-info.component';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';


@NgModule({
  declarations: [
    MainContentComponent,
    InfoComponent,
    VaccinationFormComponent,
    VaccinationListComponent,
    VaccinationAddNewComponent,
    PatientsPortalComponent,
    RegistretionFormComponent,
    OrganizationUnitComponent,
    AddressComponent,
    GoogleMapComponent,
    DoctorsInfoComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    UserRoutingModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDZ4NXYmxDz0MbnJ-hpOaMb2FTj_VXmOPU'
    }),
    TranslateModule,
  ]
})
export class UserModule { }
