import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfoComponent } from './components/info/info.component';
import { MainContentComponent } from './components/main-content/main-content.component';
import { VaccinationFormComponent } from './components/vaccination-form/vaccination-form.component';
import { VaccinationListComponent } from './components/vaccination-list/vaccination-list.component';
import { VaccinationAddNewComponent } from './components/vaccination-add-new/vaccination-add-new.component';
import { PatientsPortalComponent } from './components/patients-portal/patients-portal.component';
import { RegistretionFormComponent } from './components/registretion-form/registretion-form.component';
import { OrganizationUnitComponent } from './components/organization-unit/organization-unit.component';

const routes: Routes = [
    { path: '', component: MainContentComponent },
    { path: 'home', component: InfoComponent },
    { path: 'vaccination-form', component: VaccinationFormComponent },
    { path: 'vaccination-list', component: VaccinationListComponent },
    { path: 'vaccination-create-new', component: VaccinationAddNewComponent },
    { path: 'patients-portal', component: PatientsPortalComponent },
    { path: 'registration-form', component: RegistretionFormComponent },
    { path: 'organization-units', component: OrganizationUnitComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule { }
