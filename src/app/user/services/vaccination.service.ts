import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IVaccination } from '../components/vaccination-list/IVaccination';

@Injectable({
  providedIn: 'root'
})
export class VaccinationServiceService {

  private vaccinationUrl: string;

  constructor(private http: HttpClient) {
    this.vaccinationUrl = 'http://localhost:8083/api/vaccination';
  }

  public findAll(): Observable<IVaccination[]> {
    return this.http.get<IVaccination[]>(this.vaccinationUrl);
  } 
  public save(vaccination: IVaccination): Observable<IVaccination> {
    return this.http.post<IVaccination>(this.vaccinationUrl,vaccination);
  }
}
