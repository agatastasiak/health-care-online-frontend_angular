import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Address } from '../components/address/address';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  private addressUrl: string;
  private addressId: number;
  
  constructor(private http: HttpClient) {
    this.addressId = 111;
    this.addressUrl = 'http://localhost:8083/api/address/' + this.addressId;
  }

  public findById(): Observable<Address>{
    return this.http.get<Address>(this.addressUrl);
  }
  
}
