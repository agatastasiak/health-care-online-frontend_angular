import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { IUser } from '../IUser';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userUrl: string;

  constructor(private http: HttpClient) {
    this.userUrl = 'http://localhost:8083/api/user';
  }

  public findAll(): Observable<IUser[]> {
    return this.http.get<IUser[]>(this.userUrl);
  }

  public save(user: FormGroup): Observable<IUser> {
    return this.http.post<IUser>(this.userUrl, user.value);
  }
}
