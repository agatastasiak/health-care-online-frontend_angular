import { Injectable } from '@angular/core';
import { OrganizationUnit } from '../components/organization-unit/organization-unit';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrganizationUnitService {

  constructor(private http: HttpClient) { }

  public findOrgUnitByCity(city: string): Observable<OrganizationUnit[]> {
    const params = new HttpParams().set('city', city);
    return this.http.get<OrganizationUnit[]>(environment.apiUrl + 'api/organizationUnit', { params: params });
  }

  public findAddressCitiesOfOrgUnits(): Observable<string[]> {
    return this.http.get<string[]>(environment.apiUrl + 'api/organizationUnit/city');
  }
}
