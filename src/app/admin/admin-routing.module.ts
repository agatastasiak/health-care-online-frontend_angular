import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminMainContentComponent } from './components/admin-main-content/admin-main-content.component';

const routes: Routes = [
  { path: '', component: AdminMainContentComponent },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
