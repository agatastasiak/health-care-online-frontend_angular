import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminMainContentComponent } from './components/admin-main-content/admin-main-content.component';
import { AdminRoutingModule } from './admin-routing.module';

@NgModule({
  declarations: [AdminMainContentComponent],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
