
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse, HttpHeaderResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { tap, catchError } from 'rxjs/operators';
import { stringify } from 'querystring';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

    constructor(public toasterService: ToastrService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req)
            .pipe(
                tap(
                    event => {
                        if (event instanceof HttpResponse) {
                            console.log(event.status);
                            if (event.status.toString() === '201') {
                                this.toasterService.success(event.status.toString(), "Successfully created");
                            }
                            console.log('event: ' ,event);
                        }
                    },
                    error => {
                        if (error instanceof HttpErrorResponse) {
                            if(error.status){
                                if (error.error.errors) {
                                    let errorMessage: string = '';
                                    error.error.errors.forEach(e => {
                                        if (e.defaultMessage) {
                                            errorMessage = errorMessage.concat(e.defaultMessage);
                                        }
                                    });
                                    this.toasterService.error(errorMessage, error.status.toString());
                                }
                                else{
                                    this.toasterService.error(error.error.error,error.status.toString());
                                }
                            }
                            else{
                                this.toasterService.error("An error occured");
                            }
                        }
                    }
                )
            )
            ;
    }

}
